import entity.Ball;
import global.Constant;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Animate extends Application{

  public static void main(String[] args){
    Application.launch(args);
  }

  @Override
  public void start(Stage stage){
    Group rootNode = new Group();
    Scene container = new Scene(rootNode, Constant.WIDTH, Constant.HEIGHT);

    Ball ball = new Ball(rootNode);
    ball.setRadius(25);
    ball.setPosition(0,0);
    ball.setVelocity(2);

    stage.setScene(container);
    stage.show();

    Duration fps = Duration.millis(1000/60);
    KeyFrame oneFrame = new KeyFrame(fps, new EventHandler(){
      @Override
      public void handle(Event arg0) {
        ball.setPosition(ball.getX() + ball.getVelocity(), 0);

        if(ball.getX() > Constant.WIDTH) {
            ball.setPosition(0,0);
        }

      }
    });

    Timeline timeline = new Timeline();
    timeline.setCycleCount(Animation.INDEFINITE);
    timeline.getKeyFrames().add(oneFrame);
    timeline.play();
  }

}
