# Membuat Timeline Animation dalam JavaFX

Sesi ini ditujukan untuk memperkenalkan cara pembuatan animasi menggunakan JavaFX.

Untuk tingkat dasar, Animasi dalam JavaFX dapat dibagi menjadi 3 bagian:

* _Transition_, dan
* _Timeline Animation_.

Timeline and Transition are subclasses of the javafx.animation.Animation class. For more information about particular classes, methods, or additional features, see the API documentation

_Timeline_ dan _Transition_ merupakan _sub-class_ dari _class_ `javafx.animation.Animation`. Namun pada sesi ini hanya akan dicontohkan mengenai _Timeline Animation_.

Untuk informasi lebih lanjut terkait _class_, _method_, dan fitur lainnya, silakan lihat _API documentation_-nya.

## Menyiapkan Aplikasi

### Dasar Aplikasi

Salin kode berikut dan simpan dengan nama file `Animation.java`.

Kode ini merupakan kode aplikasi JavaFX minimal yang dapat dikompilasi.

```java
import javafx.application.Application;
import javafx.stage.Stage;

public class Animate extends Application{

  public static void main(String[] args){
    Application.launch(args);
  }

  @Override
  public void start(Stage stage){}

}
```

Lakukan kompilasi dan jalankan dengan perintah berikut:

```
javac Animation.java
java Animation
```

Saat `java Animation` dijalankan, terminal atau command prompt akan tampak diam karena aplikasi belum memiliki apapun untuk ditampilkan. `Ctrl + C` untuk menghentikan aplikasi.

### Tampilan Aplikasi

Tambahkan potongan kode berikut ke dalam fungsi `start()`, untuk menampilkan kotak dialog aplikasi.

```java
    Group rootNode = new Group();
    Scene container = new Scene(rootNode, 500, 500);

    stage.setScene(container);
    stage.show();
```

Jangan lupa untuk menambahkan library Group dan Scene di awal file.

```java
import javafx.scene.Group;
import javafx.scene.Scene;
```

Lakukan kompilasi dan jalankan, seharusnya aplikasi sudah dapat menampilkan kotak dialog aplikasi.

### Menampilkan Objek

Dalam kasus ini, kita akan menggunakan lingkaran sebagai objek yang akan dianimasikan. Tambahkan potongan kode berikut ke dalam fungsi `start()`, untuk menampilkan lingkaran.

```java
    Circle ball = new Circle();
    ball.setRadius(25);
    ball.setLayoutX(0 + 25);
    ball.setLayoutY(0 + 25);

    rootNode.getChildren().add(ball);
```

Tambahkan juga library yang diperlukan:

```java
import javafx.scene.shape.Circle;
```
## Timeline Animation

Animasi digerakkan oleh _property_ yang berkaitan dengan suatu objek, seperti ukuran, lokasi, warna, dsb. _Timeline_ memungkinkan suatu objek di-_update_ _property_-nya seiring berjalannya waktu. JavaFX mendukung _key frame animation_. Dalam _key frame animation_, jika transisi animasi dimulai dari titik awal hingga titik akhir dideklarasi, sistem dapat menjalankan animasi secara otomatis. Jika diperlukan, animasi dapat dihentikan, di-_pause_, di-_resume_, dan _reverse_, atau diulangi.

Kode berikut akan menggerakkan objek lingkaran secara horizontal dari posisi awal (x = 25) ke posisi x = 225 dalam 500ms. Letakkan dalam fungsi `start()`.

```java
    final Timeline timeline = new Timeline();
    timeline.setCycleCount(Timeline.INDEFINITE);
    timeline.setAutoReverse(true);

    final KeyValue kv = new KeyValue(ball.centerXProperty(), 225);
    final KeyFrame kf = new KeyFrame(Duration.millis(500), kv);

    timeline.getKeyFrames().add(kf);
    timeline.play();
```

Library yang diperlukan:

```java
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.util.Duration;
```

## Object Oriented Approach

Dalam bagian ini, akan digunakan pendekatan berorientasi objek dalam membangun animasi menggunakan JavaFX. Dengan menggunakan _object oriented programming_, akan didapatkan kontrol yang lebih baik terhadap setiap objek yang ada di aplikasi.

Sebelum dilanjutkan, perlu dipahami istilah `package` dalam bahasa pemrograman Java. _Package_ merupakan cara untuk mengelompokkan _class_ dan _interface_ yang berkaitan. Dalam bahasa lain, _package_ disebut dengan _namespace_.

### Membuat Objek Bola

Kode program di bawah ini merupakan contoh untuk membuat class untuk objek bernama `Ball` (bola), disertai dengan _method_ untuk mengatur ukuran (_radius_), posisi, dan kecepatan (_velocity_).

```java
import javafx.scene.shape.Circle;

public class Ball {

    Circle circle = new Circle();

    float velocity;
    float x, y;
    float radius;

    public Ball() {
        setPosition(0, 0);
        setRadius(0);
        setVelocity(0);
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
        circle.setRadius(radius);
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;
        circle.setLayoutX(x + radius);
        circle.setLayoutY(y + radius);
    }

    public float getVelocity() {
        return velocity;
    }

    public void setVelocity(float velocity) {
        this.velocity = velocity;
    }

}
```

### Membuat Package untuk Objek Bola

Simpan _file_ `Ball.java` ke dalam folder bernama `entity`. Kemudian sisipkan kode berikut di baris pertama _file_ `Ball.java`.

```java
package entity;
```

Sekarang, class `Ball` dapat dipanggil dengan sintaks berikut:

```java
import entity.Ball;
```

### Menggunakan Objek Bola

Import objek `Ball` dari file utama `Animate.java`.

Kemudian ganti `ball` hasil instansiasi objek `Circle` di method `start()` dalam class `Animate`, dengan objek `Ball` yang baru saja dibuat.

```diff
-    Circle ball = new Circle();
+    Ball ball = new Ball();
     ball.setRadius(25);
-    ball.setLayoutX(0 + 25);
-    ball.setLayoutY(0 + 25);
+    ball.setPosition(0,0);
+    bola.setVelocity(2);
```

Setelah objek `ball` diganti, library `Circle` juga dapat dihapus dari file `Animate.java`

```diff
-import javafx.scene.shape.Circle;
```

Sampai pada tahap ini, kompilasi akan gagal karena objek `Ball` tidak memiliki fungsi `centerXProperty()` yang digunakan untuk menggerakkan lingkaran pada animasi dalam setiap `KeyFrame`.

### Menambahkan Objek saat Instansiasi

Objek dapat ditambahkan ke `Group` saat instansiasi objek dilakukan. Perbarui kode di file `Ball.java` hingga seperti berikut:

```diff
 package entity;

+import javafx.scene.Group;
 import javafx.scene.shape.Circle;

 public class Ball {
     float x, y;
     float radius;

-    public Ball() {
+    public Ball(Group group) {
         setPosition(0, 0);
         setRadius(0);
         setVelocity(0);
+        group.getChildren().add(circle);
     }
```

Dan file `Animate.java` hingga seperti berikut:

```diff
-    Ball ball = new Ball();
+    Ball ball = new Ball(rootNode);
     ball.setRadius(25);
     ball.setPosition(0,0);

-    rootNode.getChildren().add(ball);
```

### KeyFrame dengan Event

Dengan pendekatan _object oriented_, objek `ball` sekarang dapat digerakkan dengan perintah dalam fungsi yang di-_pass_ ke setiap `KeyFrame`. Ganti kode di `Animate.java` sehingga seperti berikut:

```diff
+import javafx.animation.Animation;
-import javafx.animation.KeyValue;
+import javafx.event.Event;
+import javafx.event.EventHandler;

...

-    final Timeline timeline = new Timeline();
-    timeline.setCycleCount(Timeline.INDEFINITE);
-    timeline.setAutoReverse(true);
-
-    final KeyValue kv = new KeyValue(ball.centerXProperty(), 225);
-    final KeyFrame kf = new KeyFrame(Duration.millis(500), kv);
-
-    timeline.getKeyFrames().add(kf);
+    Duration fps = Duration.millis(1000/60);
+    KeyFrame oneFrame = new KeyFrame(fps, new EventHandler(){
+      @Override
+      public void handle(Event arg0) {
+        ball.setPosition(ball.getX() + ball.getVelocity(), 0);
+      }
+    });
+
+    Timeline timeline = new Timeline();
+    timeline.setCycleCount(Animation.INDEFINITE);
+    timeline.getKeyFrames().add(oneFrame);
```

### Mengulang pergerakan Bola

Dengan pengondisian, object `ball` dapat dipindahkan orientasi geraknya. Kondisi berikut akan melakukan _reset_ terhadap posisi `ball` ke koordinat x = 0, saat bola bergerak menyentuh 500px secara horizontal. Letakkan kode di dalam fungsi `handle()` milik objek `EventHandler` anonim saat pendeklarasian `KeyFrame`.

```java
        if(bola.getX() > 500) {
            bola.setPosition(0,0);
        }
```

### Konstanta dalam Java

Untuk membuat konstanta atau variable global dalam bahasa Java, dapat dibuat class `Constant` seperti berikut:

```java
public class Constant{
    public static final float WIDTH = 500;
    public static final float HEIGHT = 500;
}
```

Simpan dalam folder `global`, dan beri sintaks package:

```java
package global;
```

#### Menggunakan Konstanta

Dalam file `Animate.java`, terdapat beberapa baris yang dapat diupdate agar menggunakan konstanta.

```diff
+import global.Constant;

...

-    Scene container = new Scene(rootNode, 500, 500);
+    Scene container = new Scene(rootNode, Constant.WIDTH, Constant.HEIGHT);

...

-        if(ball.getX() > 500) {
+        if(ball.getX() > Constant.WIDTH) {
```

## Referensi

* [Animation Basics](https://docs.oracle.com/javase/8/javafx/visual-effects-tutorial/basics.htm), bagian dari _JavaFX: Transformations, Animations, and Visual Effects_.
* [Package](https://docs.oracle.com/javase/tutorial/java/package/index.html)
